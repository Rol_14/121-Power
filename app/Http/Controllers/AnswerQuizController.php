<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AnswerQuizFormRequest;
use App\QuizPrivacy;
use App\Quiz;
use App\LQAnswer;
use App\Question;
use App\Option;
use Session;
use Auth;
use App\User;
use DB;
use App\Events\NextQuestion;
use App\Events\UpdateTime;
use App\Quotation;
use URL;
use Illuminate\Support\Facades\Request as Request1;

class AnswerQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $quizIDs = QuizPrivacy::where('user_email', '=', $user->email)->get(['quiz_id']);
        $quizzes = Quiz::whereIn('id', $quizIDs)->paginate(10);
        // dd($quizzes);
        return view('answerquizzes.index', compact('quizzes'));     

    }
    public function welcome($id){
        $user = Auth::user()->id;
        $quiz_owner = Quiz::findOrFail($id)->user_id;
        return view('answerquizzes.welcome', compact('id', 'user', 'quiz_owner'));
    }

    public function endQuiz($id){
        $user = Auth::user()->id;
        $quiz_owner = Quiz::findOrFail($id)->user_id;
        LQAnswer::where('quiz_id', $id)->delete();
        return view('answerquizzes.welcome', compact('id', 'user', 'quiz_owner'));
    }
    //check answers if 

    public function updateQuestion() {
        $id = $_GET['id'];
        
        //query ha quiz gamit an quiz id
        $quiz = Quiz::with('questions')->findOrFail($id);
        $curr = $quiz->curr_question;
        $round = $quiz->curr_round;
        $questions = $quiz->questions()->where('round', '=', $round)->get();
        $isMsg = false;
        $showing = $quiz->showing;
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        if($quiz->status==0){
            $isMsg = true;
            $msg = "The quiz has already ended or it has not yet started. Please check the quiz's schedule.";
            $question=array("question"=>$msg,"isMsg"=>$isMsg);
            $question = json_encode($question);
            $options = json_encode(array());
        }else if($showing == 1){
            $isMsg = true;
            $msg = $showing;
            $question=array("question"=>$msg,"isMsg"=>$isMsg);
            $question = json_encode($question);
            $options = json_encode(array());
        }
        else{
            if($curr>count($questions)){
                $isMsg = true;
                $msg = "Please wait for the next round to start.";
                $question=array("question"=>$msg,"isMsg"=>$isMsg);
                $question = json_encode($question);
                $options = json_encode(array());
            }
            else{
                $question = $questions[$curr-1];
                $question->isMsg = $isMsg;
                $options = $question->options;
                $question->question_no = $curr;
                $question->round = $round;
            }
        }
        echo "data: {$question}\n\n";
        echo "data: {$options}";
        echo "data: {$showing}";
        flush();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // 
       

    }
    public function createAnswers(Request $request, $quiz_id){
        $answers = $request->all();
        $user_id = Auth::user()->id;
        foreach($answers as $question_id => $answer_id){
            if($question_id != "_token"){
                $points = Question::points($question_id);
                // dd($points, $question_id, $answer_id);
                $lq_answers = LQAnswer::where('quiz_id', $quiz_id)
                ->where('question_id', $question_id)
                ->where('user_id', $user_id )
                ->first();
                if(count($lq_answers) > 0){
                    $lq_answers->answer_id = $answer_id;
                    $lq_answers->points = $points;
                    $lq_answers->save();
                }else{
                    $lq_row = new LQAnswer();
                    $lq_row->user_id = $user_id;
                    $lq_row->quiz_id = $quiz_id;
                    $lq_row->question_id =  $question_id;
                    $lq_row->answer_id = $answer_id;
                    $lq_row->points =$points;
                    $lq_row->save();    
                }
            }
        }
        return view('answerquizzes.user_answers');
    }

    public function createAnswer(Request $request)
    { 
        $user_id = $request->input("user_id");
        $answer_id = $request->input("answer");
        $quiz_id =  $request->input("quiz_id");
        $curr_round =  $request->input("round");
        $question_id = $request->input("question_id");
        $sagot =  $request->input("sagot");
        if($curr_round != "Difficult" ){
           $answer = Option::where('id', $answer_id)->first()->answer;
        }
        $points = 0; 
        
        if($answer_id == 1){
            $points = 0; 
        }
        if($curr_round == "Easy" && $answer == 1){
            $points = 1;
        }elseif($curr_round == "Average" && $answer == 1){
            $points = 3;
        }elseif($curr_round == "Difficult" ){
           $points = 0;
        }    
        $lq_answers = LQAnswer::where('quiz_id', $quiz_id)
        ->where('question_id', $question_id)
        ->where('user_id', $user_id )
        ->first();
        if(count($lq_answers) > 0){
            $lq_answers->answer_id = $answer_id;
            $lq_answers->points = $points;
            $lq_answers->answer = $sagot;
            $lq_answers->save();
        }else{
            $lq_row = new LQAnswer();
            $lq_row->user_id = $user_id;
            $lq_row->quiz_id = $quiz_id;
            $lq_row->question_id =  $question_id;
            $lq_row->answer_id = $answer_id;
            $lq_row->points =$points;
            $lq_row->save();    
        }
       return "". $equal;

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


     public function storeAnswer(Request $request){
        $id = $request->input("id");
        $user = Auth::user();
        $quiz = Quiz::findOrFail($request->input("id"));
        foreach($quiz->questions as $question){
                 $lq_answers = LQAnswer::where('quiz_id', $id)
                ->where('question_id', $question->id)
                ->where('user_id', $user->id )
                ->first();

                if(count($lq_answers) > 0){
                    $lq_answers->answer_id = 0;
                    $lq_answers->points = 0;
                    $lq_answers->save();
                }else{
                    $lq_row = new LQAnswer();
                    $lq_row->user_id = $user->id;
                    $lq_row->quiz_id = $id;
                    $lq_row->question_id =  $question->id;
                    $lq_row->answer_id = 0;
                    $lq_row->points = 0;
                    $lq_row->save();    
                }
        }
        if ($quiz->quiz_type==0) {
            return view('answerquizzes.conventional_quiz', compact('quiz'));
        }
        elseif ($quiz->quiz_type==1) {
            return redirect()->route('answerquizzes.show', ['id' => $request->input("id")]);      
        }
       
     }


     public function show($id)
    {
        $user = Auth::user();
        $quiz = Quiz::findOrFail($id);
        if ($quiz->quiz_type==0) {
            return view('answerquizzes.conventional_quiz', compact('quiz'));
        }
        elseif ($quiz->quiz_type==1) {     
            return view('answerquizzes.live_quiz', compact('quiz', 'try', 'user'));   
        }
    }
    public function administerQuiz($id){
        $quiz = Quiz::findOrFail($id);
        $quiz->curr_question = 0;
        $quiz->status = 1;
        $quiz->curr_round = "Easy";
        $quiz->showing = 0;
        $quiz->save();
        return $this->getNext($id);
    }

    public function nextRound($id){
        $quiz = Quiz::findOrFail($id);
        if ($quiz->status==1) {
            if($quiz->curr_round=="Difficult"){
            $quiz->status = 0;
            $quiz->curr_round = "Easy";
            $quiz->save();
            $lq_answers = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->where('quiz_id',$id )->orderBy('user_id')->groupBy('user_id')->get();
            $pairs = array();
            $curr_questions = $quiz->questions()->where('round', '=', 'Easy')->get(['question_id']);
            $easyA = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->whereIn('question_id', $curr_questions->pluck('question_id'))->where('quiz_id',$id )->orderBy('user_id')->groupBy('user_id')->get();
            $curr_questions = $quiz->questions()->where('round', '=', 'Average')->get(['question_id']);
            $mediumA = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->whereIn('question_id', $curr_questions->pluck('question_id'))->where('quiz_id',$id )->orderBy('user_id')->groupBy('user_id')->get();
            $curr_questions = $quiz->questions()->where('round', '=', 'Difficult')->get(['question_id']);
            $difficultA = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->whereIn('question_id', $curr_questions->pluck('question_id'))->where('quiz_id',$id )->orderBy('user_id')->groupBy('user_id')->get();
              
            for($i = 0; $i < count($lq_answers); $i++) {
                $username = User::where('id', $lq_answers[$i]['user_id'])->first()->name;
                $points = $lq_answers[$i]->points;
                $easy = $easyA[$i]->points;
                $medium = $mediumA[$i]->points;
                $difficult = $difficultA[$i]->points;
                $pair = array(
                    'username' => $username."",
                    'points' => $points."",
                    'easy' => $easy."",
                    'medium' => $medium."",
                    'difficult' => $difficult."",

                );
                array_push($pairs, $pair);  
              }
              //dd($pairs); 
              return view('answerquizzes.end_quiz', compact('pairs', 'id'));
        }
        else{
            if($quiz->curr_round=="Easy"){
                $quiz->curr_round = "Average";
            }
            elseif($quiz->curr_round=="Average"){
                $quiz->curr_round = "Difficult";
            }
            $quiz->curr_question = 0;
            $quiz->save();
            return $this->getNext($id);
        }
            # code...
        }
        else{
            $quiz->curr_question = 0;
            $quiz->status = 1;
            $quiz->curr_round = "Easy";
            $quiz->save();
            return $this->getNext($id);
        }
    }
    public function getNext($id){
//         $time = '30:10';
// if (substr_count($time, ':') === 1) $time = '00:'.$time;
// $parsed = date_parse($time);
// $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];


// dd($seconds);
        $seconds =0;
        $endRound = false;
        $quiz = Quiz::findOrFail($id);
        
        if(null !== Request1::server('HTTP_REFERER') || URL::previous() == URL::route('quizzes.administer', array('id'=> $id))){
             $curr = $quiz->curr_question + 1;
             $quiz->curr_question = $curr;
        }
        // dd(URL::previous() != URL::full());
        // if(URL::previous() != URL::full()){
        //    $quiz->curr_question = $curr;
        // }
        $questions = $quiz->questions()->where('round', '=', $quiz->curr_round)->get();
        $curr_questions = $quiz->questions()->where('round', '=', $quiz->curr_round)->get(['question_id']);
        $lq_answers = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->whereIn('question_id', $curr_questions->pluck('question_id'))->where('quiz_id',$id )->orderBy('user_id')->groupBy('user_id')->get();
        $lq_answers1 = LQAnswer::select(DB::raw('SUM(points) as points, user_id'))->where('quiz_id',$id )
        ->orderBy('user_id')->groupBy('user_id')->get();
        $pairs = array();
        //dd($lq_answers);   
        for($i = 0; $i < count($lq_answers); $i++) {  
            $username = User::where('id', $lq_answers[$i]['user_id'])->first()->name;
            $points = $lq_answers1[$i]->points;
            $curr_roundPoints = $lq_answers[$i]->points;
            $pair = array(
                'username' => $username."",
                'points' => $points."",
                'round_points' => $curr_roundPoints ."",
            );
            array_push($pairs, $pair);  
          }
        if($quiz->curr_question>count($questions)){
            $endRound = true;
             event(new NextQuestion($id, 0, "", "" ));
        }

        else{
            $question = $questions[$quiz->curr_question-1];
            $time = $question->time;
            if (substr_count($time, ':') === 1) $time = '00:'.$time;
            $parsed = date_parse($time);
            $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
            $options = $question->options;
            event(new NextQuestion($id, $question, $options,$quiz->curr_question ));
        }
        $quiz->showing=0;
        $quiz->save();
       
        return view('answerquizzes.quiz_master', compact('id', 'quiz', 'question', 'endRound', 'options', 'pairs', 'seconds'));
    }

    public function showCorrectAnswer($quizID, $questionID){
        event(new NextQuestion($quizID, -1, "", "" ));
        $quiz = Quiz::where('id', '=', $quizID)->first();
        $quiz->showing = 1;
        $quiz->save();
        $answer = "";
        if($quiz->curr_round!="Difficult"){
            $answer = Option::where('question_id', $questionID)->where('answer', 1)->first()->value;
        }
        else{
            $answer = Question::where('id', $questionID)->first()->answer;
        }
        $answer = strip_tags($answer);
        return view('answerquizzes.correct_answer', compact('quizID', 'questionID', 'answer'));
    }
    public function showAnswer($quizID, $questionID){
        $answers = LQAnswer::where('quiz_id', '=', $quizID)->where('question_id', '=', $questionID)->get();
        $quiz = Quiz::where('id', '=', $quizID)->first();
        $question = Question::findOrFail($questionID);
        $quiz->save();
        $pairs = array();
        foreach ($answers as $userAnswer) {                
                $answer = $userAnswer->answer;
                $username = User::where('id', $userAnswer['user_id'])->first()->name;
                $check = 0;
                $pair = array(
                    'points' => $userAnswer['points']."",
                    'user_id' => $userAnswer['user_id']."",
                    'username' => $username."",
                    'answer' => $answer."",
                    'check' => $check
                );
                array_push($pairs, $pair);  
            // dd($round);
            }
        return view('answerquizzes.show_answers', compact('answers', 'quizID', 'quiz', 'question', 'pairs'));
    }
    public function mark_answer($id, $round, $question_id, $check, $user, $question_number) {

        $lq_answers = LQAnswer::where('quiz_id', $id)
            ->where('question_id', $question_id)
            ->where('user_id', $user)
            ->first();

        $lq_answers->points = (int)$check;
        $lq_answers->save();

        return redirect()->route('quizzes.showanswer', compact('id', 'question_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateTime(Request $request){
        $time = $request->input('time');
        $id = $request->input('id');
        event(new UpdateTime($id, $time));
        
    }
    // public function answerQuiz($id){

    // }
    // public function administerQuiz($quizID){
    //     $quizID;

    // }
}
