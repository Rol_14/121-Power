<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionFormRequest;

use App\Question;
use App\Quiz;
use Illuminate\Http\Request;
use Auth;



class QuestionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$user = Auth::user();
		$questions = Question::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10);


		return view('questions.index', compact('questions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$question = new Question();
		return view('questions.create', compact('question'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(QuestionFormRequest $request)
	{
		$question = new Question();
		$user = Auth::user();
		$question->question = $request->input("question");
		$question->round = $request->input("round");
		$question->user_id = $user->id;
		$question->answer = $request->input("answer");
		$question->save();
        if($question->round == "Difficult"){
		    return redirect()->route('questions.index', compact('question'))->with('message', 'Item created successfully.');
		}else{
		    return redirect()->route('questions.edit', compact('question'))->with('message', 'Item created successfully.');
		}
	} 

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($question)
	{
		$question = Question::findOrFail($question);
		return view('questions.show', compact('question'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($question)
	{
		$question = Question::findOrFail($question);
		return view('questions.edit', compact('question'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(QuestionFormRequest $request, $question)
	{
		$question = Question::findOrFail($question);

		$question->question = $request->input("question");
        $question->round = $request->input("round");
        $question->answer = $request->input("answer");
        $question->points = $request->input("points");
		$question->save();

		return redirect()->route('questions.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($question)
	{
		$question = Question::findOrFail($question);
		$question->delete();

		return redirect()->route('questions.index')->with('message', 'Item deleted successfully.');
	}

}
