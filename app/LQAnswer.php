<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LQAnswer extends Model
{
    protected $table = "lq_answers";
}
