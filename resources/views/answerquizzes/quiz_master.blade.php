@extends('layout')
<style>
	/* .w {
		font-family: 'Josefin Sans', sans-serif;
		font-size: 20px;
		font-weight: 800;
		
	}
	.ss {
		font-family: 'Josefin Slab', serif;
		font-size: 18px;
		border: 0;
		border-bottom: 2px solid rgba(0,0,0, 0.3);
	} */

</style>
<script>
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2018 15:37:25").getTime();
var distance = {{ $seconds }};
var startId = null;
// Update the count down every 1 second
function x() { 
   var op = document.getElementById("st").innerHTML;
   if(op === "Start"){
	document.getElementById("st").innerHTML = "Stop";
	startId = setInterval(function() {

	// Get todays date and time
	var now = new Date().getTime();

	// Find the distance between now an the count down date
	
	// Time calculations for days, hours, minutes and seconds
	var days = Math.floor(distance / (60 * 60 * 24));
	var hours = Math.floor((distance % ( 60 * 60 * 24)) / ( 60 * 60));
	var minutes = Math.floor((distance % ( 60 * 60)) / (60));
	var seconds = Math.floor((distance % (60)));

	// Display the result in the element with id="demo"
	document.getElementById("demo").innerHTML = days + "d " + hours + "h "
	+ minutes + "m " + seconds + "s ";
	var time = distance;
	$.ajax({
			type: "POST",
			url: "{{ route('quizzes.updateTime'	) }}" ,
			data: { "_token": "{{ csrf_token() }}", id: {{ $quiz->id }}, time: time}
		})

	// If the count down is finished, write some text 
	if (distance <= 0) {
		clearInterval(startId);
		document.getElementById("demo").innerHTML = "EXPIRED";
	}
	distance -= 1;
	}, 1000);
   }else{
	    document.getElementById("st").innerHTML = "Start";
	   clearInterval(startId);  
   }
}
</script>

@if(!$endRound)
	@section('header')
		<br><div class="row">
	      <div class ="col-md-8">
		      <h3 class="fnt note rounded" style="width: 55%;">
			      <span >{{ $quiz->curr_round }} Round</span> 
			      Question No. {{$quiz->curr_question}} 
		      </h3>
	      </div>
	 	</div>
	@endsection

	@section('content')<br><br>
	<button id="st" onclick=x();>Start</button>
    <p id="demo"></p>

	<div class="panel panel-success">
        <div class="panel-heading w">{!! $question->question !!}</div>
			<?php $let=65; ?>
			@foreach($options as $option)
			<div class="sa panel-body ss" style="padding: 8px;">
				<h4>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo chr($let++) ?>.&nbsp;&nbsp;{{strip_tags($option->value)}}<h4>
			</div>
			@endforeach
	</div><br>
	<a class="bb" style="margin: 0px 40%" value="Show Answer" href="{{ route('quizzes.showCorrectAnswer', [$quiz->id, $question->id]) }}">Show Answer</a>
	
	@endsection
@else
	@section('header')
	<h2 class="fnt note rounded" style="width: 33%">End of {{$quiz->curr_round}} Round</h2>
	<br>
    <div class="row">
        <div class="col-md-12 tbl-header">
			<table class="tbl" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th>Name of Participant</th>
						<th style="text-align: center">Points for {{$quiz->curr_round}}</th>
						<th style="text-align: center">Current Points</th>
					</tr>
				</thead>
            </table>
        </div>

        <div class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
            	<tbody>
					@foreach($pairs as $pair)
					<tr>
						<td>&nbsp;&nbsp;&nbsp;{!! $pair['username'] !!}</td>
						<td style="text-align: center">{!! $pair['round_points'] !!}</td>
						<td style="text-align: center">{!! $pair['points'] !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div><br>

		<a class="btn btn-success" href="{{ route('quizzes.nextround', $quiz->id) }}">Next Round</a>
	@endsection
@endif