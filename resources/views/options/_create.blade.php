
<style>
.wlll {
        padding: 7px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        width: 15.5%;
        margin: 20px auto;
    }
    </style>
    <div class="panel panel-default">
    <div class="panel-body">

<div class="row">
    <div class="col-md-12">

        <form action="{{ route('questions.options.store', ['question'=>$question->id]) }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label style="font-size: 15px;" for="question-field">CREATE ANOTHER OPTION?</label>
            @include('options._option')

            <div class="wlll well-sm">
                <button type="submit" class="btn btn-success">Create</button>
                <a class="btn btn-primary" href="{{ route('questions.show', $question->id) }}"><i class="glyphicon glyphicon-backward"></i> Back</a>

            </div>
        </form>

    </div>
</div>
</div>
</div>
