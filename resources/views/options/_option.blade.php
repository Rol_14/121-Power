
<style>
  .bttnn {
      position: absolute;
      top: 5px;
      right: 170px;
   }
   .bt {
      position: absolute;
      top: 0px;
      right: 110px;
   }  
   .btt {
      position: absolute;
      top: 0px;
      right: 54px;
   }
  
</style>
<div class="form-group @if($errors->has('answer')) has-error @endif">
   <label class="bttnn" for="answer-field">Is the below answer?</label>
    <div data-toggle="buttons">
      <label class="btn bt btn-success {{$option->answer == 1 ? 'active': ''}}">
        <input type="radio" value="1" name="answer" id="answer-field" autocomplete="off" {{$option->answer == 1 ? 'checked': ''}}> 
          True
      </label>
      <label class="btn btt btn-warning {{$option->answer == 0 ? 'active': ''}}">

        <input type="radio" value="0" name="answer" id="answer-field" autocomplete="off" {{$option->answer == 0 ? 'checked': ''}}> 
          False
      </label>
    </div>
   @if($errors->has("answer"))
    <span class="help-block">{{ $errors->first("answer") }}</span>
   @endif

</div>
<div class="form-group @if($errors->has('value')) has-error @endif">
   <label for="value-field">Value</label>
<textarea id="value-field" name="value" class="form-control">
  {{ is_null(old("value")) ? $option->value : old("value") }}
</textarea>
   @if($errors->has("value"))
    <span class="help-block">{{ $errors->first("value") }}</span>
   @endif
</div>
