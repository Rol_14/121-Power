@extends('layout')

@section('header')
    <div class="page-hdr clearfix">
        <h1 class="fnt note rounded" style="width: 25%"><i class="glyphicon glyphicon-align-justify" aria-hidden="true"></i> QUESTIONS</h1> 
         	<form class="form-inline pull-right">
		        <div class="form-group">
		          <input type="text" class="form-control input-sm" id="search-field" placeholder="Search">
		        </div>
		        <div class="form-group">
		        	<button type="submit" class="btn input-sm btn-default">Search</button>
			        <a class="btn input-sm btn-success" href="{{ route('questions.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
			    </div>  
	      	</form>
    </div><br>
@endsection

@section('content')
    @include('questions._index')
@endsection