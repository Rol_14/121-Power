<div class="form-group @if($errors->has('name')) has-error @endif">
  <label for="name-field">Name</label>
  <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $quiz->name : old("name") }}"/>
  @if($errors->has("name"))
    <span class="help-block">{{ $errors->first("name") }}</span>
  @endif
  <label for="schedule-field">Schedule</label>
 <div class="input-group date">
  <input type="text" class="form-control" id="schedule-field" name="schedule" value="{{ is_null(old("schedule")) ? $quiz->schedule : old("schedule") }}"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
</div>
  @if($errors->has("schedule"))
    <span class="help-block">{{ $errors->first("schedule") }}</span>
  @endif                  
</div>
