@extends('layout')

@section('header')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    @endif
    <div class="page-hdr clearfix">
        <h1 class="fnt note rounded">
        <i class="glyphicon glyphicon-menu-down" aria-hidden="true"></i>QUIZZES
        </h1> 
            <form class="form-inline pull-right">
        <div class="form-group">
          <input type="text" class="form-control input-sm" id="search-field" placeholder="Search">
        </div>
        <div class="form-group">
            <button type="submit" class="btn input-sm btn-default">Search</button>
            <a class="btn btn-success input-sm" href="{{ route('quizzes.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </div>  
    </form>
    </div>
@endsection

@section('content')
    </br> <div class="row">
    @if($quizzes->count())
        <div class="col-md-12 tbl-header">
           
                <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>Schedule</th>
                            <th>OPTIONS</th>
                        </tr>
                    </thead>
                </table>
        </div>

        <div class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        @foreach($quizzes as $quiz)
                            <tr>
                                <td>{{$quiz->id}}</td>
                                <td>{{$quiz->name}}</td>
                                <td>{{$quiz->schedule}}</td>
                                <td>
                                    <a class="btn btn-xs btn-primary" href="{{route('answerquizzes.welcome', $quiz->id)}}"><i class="glyphicon glyphicon-eye-open"></i>Start Live Quiz</a>
                                    <a class="btn btn-xs btn-primary" href="{{ route('quizzes.show', $quiz->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('quizzes.edit', $quiz->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('quizzes.destroy', $quiz->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $quizzes->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection