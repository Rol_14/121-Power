@extends('layout')
@section('header')
<div class="page-header">
    <h1 class="fnt note rounded" style="width: 30%">
        <i class="glyphicon glyphicon-stats" aria-hidden="true"></i> {{$quiz->name}}
    </h1> 
    <a style="position: absolute; right: 190px; top: 150px;" class="btn btn-primary" href="{{ route('quizzes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
        <form action="{{ route('quizzes.destroy', $quiz->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <a style="position: absolute; right: 270px; top: 150px;" class="btn btn-success" href="{{ route('quizzes.administer', $quiz->id) }}">Start Quiz</a>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">Questions</div>
            <div class="panel-body">
              @foreach($quiz->questions as $question)
                @include('questions._show', ['question' => $question])
              @endforeach
            </div>
        </div>
      </div>
    </div>
@endsection